package com.example.schoolnotificationdemo.entity;

import com.google.gson.annotations.SerializedName;

public class ParentResponse {

    @SerializedName("profile")
    private ProfileEntity profile;
    @SerializedName("config")
    private ConfigEntity config;
    @SerializedName("features")
    private FeaturesEntity features;
    @SerializedName("code")
    private String code;
    @SerializedName("token")
    private String token;
    @SerializedName("result")
    private String result;

    public ProfileEntity getProfile() {
        return profile;
    }

    public void setProfile(ProfileEntity profile) {
        this.profile = profile;
    }

    public ConfigEntity getConfig() {
        return config;
    }

    public void setConfig(ConfigEntity config) {
        this.config = config;
    }

    public FeaturesEntity getFeatures() {
        return features;
    }

    public void setFeatures(FeaturesEntity features) {
        this.features = features;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public static class ProfileEntity {

        @SerializedName("photo")
        private String photo;
        @SerializedName("name")
        private String name;
        @SerializedName("id")
        private String id;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "ProfileEntity{" +
                    "photo='" + photo + '\'' +
                    ", name='" + name + '\'' +
                    ", id='" + id + '\'' +
                    '}';
        }
    }

    public static class ConfigEntity {
        @SerializedName("bus_interval")
        private String bus_interval;

        public String getBus_interval() {
            return bus_interval;
        }

        public void setBus_interval(String bus_interval) {
            this.bus_interval = bus_interval;
        }
    }

    public static class FeaturesEntity {
        @SerializedName("staff_chat")
        private int staff_chat;
        @SerializedName("master_acc")
        private int master_acc;
        @SerializedName("timeline")
        private int timeline;
        @SerializedName("chat")
        private int chat;

        public int getStaff_chat() {
            return staff_chat;
        }

        public void setStaff_chat(int staff_chat) {
            this.staff_chat = staff_chat;
        }

        public int getMaster_acc() {
            return master_acc;
        }

        public void setMaster_acc(int master_acc) {
            this.master_acc = master_acc;
        }

        public int getTimeline() {
            return timeline;
        }

        public void setTimeline(int timeline) {
            this.timeline = timeline;
        }

        public int getChat() {
            return chat;
        }

        public void setChat(int chat) {
            this.chat = chat;
        }
    }

}
