package com.example.schoolnotificationdemo.entity;

import java.util.List;

public class Parent {

   private String codePassword;
   private String phoneNumber;
   private String LoginType;
   private String deviceKey;
   private String typeOS;
   private String appId;

   public Parent(){

   }

    public Parent(String codePassword, String phoneNumber, String loginType, String deviceKey, String typeOS, String appId) {
        this.codePassword = codePassword;
        this.phoneNumber = phoneNumber;
        LoginType = loginType;
        this.deviceKey = deviceKey;
        this.typeOS = typeOS;
        this.appId = appId;
    }



    public String getCodePassword() {
        return codePassword;
    }

    public void setCodePassword(String codePassword) {
        this.codePassword = codePassword;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLoginType() {
        return LoginType;
    }

    public void setLoginType(String loginType) {
        LoginType = loginType;
    }

    public String getDeviceKey() {
        return deviceKey;
    }

    public void setDeviceKey(String deviceKey) {
        this.deviceKey = deviceKey;
    }

    public String getTypeOS() {
        return typeOS;
    }

    public void setTypeOS(String typeOS) {
        this.typeOS = typeOS;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @Override
    public String toString() {
        return "Parent{" +
                "codePassword='" + codePassword + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", LoginType='" + LoginType + '\'' +
                ", deviceKey='" + deviceKey + '\'' +
                ", typeOS='" + typeOS + '\'' +
                ", appId='" + appId + '\'' +
                '}';
    }
}
