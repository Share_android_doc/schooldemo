package com.example.schoolnotificationdemo.entity;

import com.google.gson.annotations.SerializedName;

public abstract class UpdateNameResponse {

    @SerializedName("code")
    private String code;
    @SerializedName("status")
    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
