package com.example.schoolnotificationdemo.entity;

import com.google.gson.annotations.SerializedName;

public class StudentResponse {


    @SerializedName("photo")
    private String photo;
    @SerializedName("end_time")
    private String endTime;
    @SerializedName("start_time")
    private String startTime;
    @SerializedName("teacher_assistant")
    private String teacherAssistant;
    @SerializedName("teacher_name")
    private String teacherName;
    @SerializedName("school_name")
    private String schoolName;
    @SerializedName("school_id")
    private String schoolId;
    @SerializedName("branch_name")
    private String branchName;
    @SerializedName("status")
    private String status;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("branch_id")
    private String branchId;
    @SerializedName("balance")
    private String balance;
    @SerializedName("room")
    private String room;
    @SerializedName("level_id")
    private String levelId;
    @SerializedName("level")
    private String level;
    @SerializedName("dob")
    private String dob;
    @SerializedName("nick_name")
    private String nickName;
    @SerializedName("name_kh")
    private String nameKh;
    @SerializedName("name_en")
    private String nameEn;
    @SerializedName("id_card")
    private String idCard;
    @SerializedName("fingerprint")
    private String fingerprint;
    @SerializedName("id_number")
    private String idNumber;
    @SerializedName("sex")
    private String sex;
    @SerializedName("id")
    private String id;

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getTeacherAssistant() {
        return teacherAssistant;
    }

    public void setTeacherAssistant(String teacherAssistant) {
        this.teacherAssistant = teacherAssistant;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public String getBranchName() {
        return branchName;
    }

    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getLevelId() {
        return levelId;
    }

    public void setLevelId(String levelId) {
        this.levelId = levelId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNameKh() {
        return nameKh;
    }

    public void setNameKh(String nameKh) {
        this.nameKh = nameKh;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getFingerprint() {
        return fingerprint;
    }

    public void setFingerprint(String fingerprint) {
        this.fingerprint = fingerprint;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "StudentResponse{" +
                "photo='" + photo + '\'' +
                ", endTime='" + endTime + '\'' +
                ", startTime='" + startTime + '\'' +
                ", teacherAssistant='" + teacherAssistant + '\'' +
                ", teacherName='" + teacherName + '\'' +
                ", schoolName='" + schoolName + '\'' +
                ", schoolId='" + schoolId + '\'' +
                ", branchName='" + branchName + '\'' +
                ", status='" + status + '\'' +
                ", endDate='" + endDate + '\'' +
                ", branchId='" + branchId + '\'' +
                ", balance='" + balance + '\'' +
                ", room='" + room + '\'' +
                ", levelId='" + levelId + '\'' +
                ", level='" + level + '\'' +
                ", dob='" + dob + '\'' +
                ", nickName='" + nickName + '\'' +
                ", nameKh='" + nameKh + '\'' +
                ", nameEn='" + nameEn + '\'' +
                ", idCard='" + idCard + '\'' +
                ", fingerprint='" + fingerprint + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", sex='" + sex + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
