package com.example.schoolnotificationdemo.ui.adapter;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.entity.StudentResponse;
import com.squareup.picasso.Picasso;


import java.util.List;


public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.MyViewHolder>{

    List<StudentResponse> studentResponseList;

    Context context;
    LayoutInflater inflater;
    StudentResponse studentResponse;

    String name, gender, school, room, level, branch, id;

    public StudentAdapter(List<StudentResponse> studentResponseList, Context context) {
        this.studentResponseList = studentResponseList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.student_list_layout, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        studentResponse = studentResponseList.get(position);
        holder.grade.setText(studentResponse.getLevel());
        holder.gender.setText(studentResponse.getSex());
        holder.name.setText(studentResponse.getNameEn());



    }




    @Override
    public int getItemCount() {
        return studentResponseList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView studentPhoto;
        TextView name, gender, grade;
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            studentPhoto = itemView.findViewById(R.id.photo);
            name = itemView.findViewById(R.id.tvName);
            gender = itemView.findViewById(R.id.tvGender);
            grade = itemView.findViewById(R.id.tvGrade);


        }
    }
}
