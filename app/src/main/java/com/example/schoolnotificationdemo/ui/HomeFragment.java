package com.example.schoolnotificationdemo.ui;

import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.api.network.ServiceGenerator;
import com.example.schoolnotificationdemo.api.service.SchoolService;
import com.example.schoolnotificationdemo.entity.ParentResponse;
import com.example.schoolnotificationdemo.entity.StudentResponse;
import com.example.schoolnotificationdemo.helper.localDatabase.databaseCreater.AppDatabase;
import com.example.schoolnotificationdemo.helper.localDatabase.model.Student;
import com.example.schoolnotificationdemo.ui.adapter.StudentAdapter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment {

    StudentAdapter studentAdapter;
    StudentResponse studentResponse;
    RecyclerView recyclerView;
    List<StudentResponse> studentResponseList;
    SchoolService.StudentService studentService;
    String TAG = HomeFragment.class.getSimpleName();
    AppDatabase database;

    List<Student> students;
    Student student;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        return inflater.inflate(R.layout.fragment_home, null);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.homeRecyclerView);
        studentService = ServiceGenerator.createService(SchoolService.StudentService.class);
        database=AppDatabase.getDatabaseInstance(getActivity());
        //insertData();
        studentResponseList = new ArrayList<>();
        students = new ArrayList<>();
        student = new Student();
        getAllStudents();

    }

    private void insertData(Student student) {

        database.studentDAO().insertStudent(student);
    }


    void getAllStudents(){

        try{

            Call<List<StudentResponse>> studentCall = studentService.getAllStudents();
            studentCall.enqueue(new Callback<List<StudentResponse>>() {
                @Override
                public void onResponse(Call<List<StudentResponse>> call, Response<List<StudentResponse>> response) {
                  try {
                      Log.e(TAG, "success" + response.body().toString());

                      for (int i=0; i<response.body().size(); i++){
                          student.setFullName(response.body().get(i).getNameEn());
                          student.setGender(response.body().get(i).getSex());
                          student.setLevel(response.body().get(i).getLevel());
                          //call one if you want to run again , please change database name
                          insertData(student);
                      }


                     // Log.e(TAG,"data"+database.studentDAO().getAll().toString());
                      studentAdapter = new StudentAdapter(response.body(), getContext().getApplicationContext());
                      recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

                      recyclerView.setAdapter(studentAdapter);
                  }catch (Exception e){
                      e.printStackTrace();
                  }
                }

                @Override
                public void onFailure(Call<List<StudentResponse>> call, Throwable t) {
                    Log.e(TAG, "error" + t.toString());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppDatabase.destroyInstance();
    }
}
