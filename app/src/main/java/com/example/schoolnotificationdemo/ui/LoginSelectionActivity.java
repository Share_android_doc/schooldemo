package com.example.schoolnotificationdemo.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.schoolnotificationdemo.R;

public class LoginSelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_selection);
    }

    public void onLoginPhone(View view) {

        Intent intent = new Intent(LoginSelectionActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void onLoginGmail(View view) {
        Toast.makeText(this, "Login with E-mail!", Toast.LENGTH_SHORT).show();
    }

    public void onLoginFacebook(View view) {
        Toast.makeText(this, "Login with Facebook!", Toast.LENGTH_SHORT).show();
    }
}
