package com.example.schoolnotificationdemo.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.api.network.ServiceGenerator;
import com.example.schoolnotificationdemo.api.service.SchoolService;
import com.example.schoolnotificationdemo.entity.Parent;
import com.example.schoolnotificationdemo.entity.ParentResponse;
import com.example.schoolnotificationdemo.helper.StorePreferences;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    CheckBox chRemember;

    SchoolService.ParentService parentService;
    Parent parent;


    EditText phone, password;
    StorePreferences storePreferences;
    byte[] enPassword;
    byte[] enPhone;
    ProgressBar progressBar;

    String TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        chRemember = findViewById(R.id.checkbox_remember);


        progressBar = findViewById(R.id.progressbar);
        storePreferences = new StorePreferences(LoginActivity.this);
        parentService = ServiceGenerator.createService(SchoolService.ParentService.class);
        phone = findViewById(R.id.etPhone);
        password = findViewById(R.id.etPassword);

        chRemember.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
               if (checked){
                   hideKeyboard(LoginActivity.this);
               }
            }
        });
        progressBar.setVisibility(View.INVISIBLE);

    }


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void onBackHome(View view) {
        startActivity(new Intent(this, LoginSelectionActivity.class));
    }


    private void loginWithPhone(){


            String code = password.getText().toString();
            enPassword = Base64.encode(code.getBytes(), Base64.DEFAULT);

           String encodePhone = phone.getText().toString();
            enPhone = Base64.encode(encodePhone.getBytes(), Base64.DEFAULT);


        String encodeAppId ="trendsecsolution.com.schoolnotificationappdev";
        byte[] enAppId = Base64.encode(encodeAppId.getBytes(), Base64.DEFAULT);

        String encodeTypeOS ="android";
        byte[] enTypeOS = Base64.encode(encodeTypeOS.getBytes(), Base64.DEFAULT);

        String encodeLoginType ="parent";
        byte[] enLoginType = Base64.encode(encodeLoginType.getBytes(), Base64.DEFAULT);

        byte[] decodeValue = Base64.decode(enAppId, Base64.DEFAULT);
        Log.e(TAG,"-->"+ new String(decodeValue));



        parent = new Parent();
        parent.setAppId(new String(enAppId));
        parent.setDeviceKey("0");
        parent.setTypeOS(new String(enTypeOS));
        parent.setLoginType(new String(enLoginType));
        parent.setCodePassword(new String(enPassword));
        parent.setPhoneNumber(new String(enPhone));


        Call<ParentResponse> parentCall = parentService.login(parent.getCodePassword(),parent.getPhoneNumber(), parent.getLoginType());
        parentCall.enqueue(new Callback<ParentResponse>() {
            @Override
            public void onResponse(Call<ParentResponse> call, Response<ParentResponse> response) {

                if (response.body().getResult().toString().equals("sucess")) {

                    new ServiceGenerator(response.body().getToken().toString());
                    ParentResponse.ProfileEntity responseProfile = response.body().getProfile();
                    storePreferences.storeStringPreference(LoginActivity.this, "user_id", responseProfile.getId());
                    storePreferences.storeStringPreference(LoginActivity.this, "user_name", responseProfile.getName());
                    storePreferences.storeStringPreference(LoginActivity.this, "user_photo", responseProfile.getPhoto());
                    storePreferences.storeStringPreference(LoginActivity.this, "token_id", response.body().getToken().toString());
                    storePreferences.storeStringPreference(LoginActivity.this, "phone_id", phone.getText().toString());



                    progressBar.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("newusername", responseProfile.getName());
                    startActivity(intent);
                    Log.e(TAG, "-->success" + response.body().getProfile().toString());
                }else {

                    phone.setText(null);
                    phone.requestFocus();
                    password.setText(null);
                    Toast.makeText(LoginActivity.this, "Your phone and password are wrong!", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ParentResponse> call, Throwable t) {
                Log.e(TAG,"-->error"+t.toString());
            }
        });

    }

    public void onLogin(View view) {

        if (TextUtils.isEmpty(password.getText().toString())){
            password.setError("Your password is blank!");
        }if (TextUtils.isEmpty(phone.getText().toString())){
            phone.setError("Your phone number is blank!");
        }else {
            loginWithPhone();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        progressBar.setVisibility(View.INVISIBLE);
    }
}

