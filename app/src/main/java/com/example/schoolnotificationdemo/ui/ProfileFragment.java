package com.example.schoolnotificationdemo.ui;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.MediaStore;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.api.network.ServiceGenerator;
import com.example.schoolnotificationdemo.api.service.SchoolService;
import com.example.schoolnotificationdemo.entity.ParentResponse;
import com.example.schoolnotificationdemo.entity.UserID;
import com.example.schoolnotificationdemo.helper.StorePreferences;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {

    private static final int OPEN_GALLERY = 1;
    private static final int READ_EXTERNAL_STORAGE_CODE = 1;
    TextView name, phone;
    StorePreferences storePreferences;
    String userId;

    Button browse, update;

    TextView editName;
    SchoolService.ParentService parentService;
    String TAG = ProfileFragment.class.getSimpleName();
    ImageView photo;
    private String imagePath;
    TextView updateimage;
    Uri uri;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, null);

    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        storePreferences = new StorePreferences(getContext().getApplicationContext());
        name = view.findViewById(R.id.tvParentName);
        phone = view.findViewById(R.id.tvPhone);
        photo = view.findViewById(R.id.photoView);
        Intent intent = getActivity().getIntent();
        String username = intent.getStringExtra("newusername");
        name.setText(username);


        browse = view.findViewById(R.id.btnBrowse);
        update = view.findViewById(R.id.btnSave);
        update.setEnabled(false);


        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final File file = new File(imagePath);

                final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

                Call<JsonObject> uploadImageCall = parentService.uploadImage(body);
                uploadImageCall.enqueue(new Callback<JsonObject>() {
                    @Override
                    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                        Log.e(TAG,"image success"+response.body().toString());
                        Toast.makeText(getActivity(), "Update profile Success", Toast.LENGTH_LONG).show();
                        Glide.with(getActivity()).load("http://pre.schnotify.info/api/user_photo?user_id="+userId)
                                .diskCacheStrategy(DiskCacheStrategy.NONE )
                                .skipMemoryCache(true)
                                .into(photo);

                        update.setEnabled(false);

                    }

                    @Override
                    public void onFailure(Call<JsonObject> call, Throwable t) {
                        Log.e(TAG,"image error"+t.toString());

                    }
                });
            }
        });



        phone.setText(storePreferences.getStringPreference(getContext(),"phone_id"));
        userId = storePreferences.getStringPreference(getContext(), "user_id");
        parentService= ServiceGenerator.createService(SchoolService.ParentService.class);
        editName = view.findViewById(R.id.editName);
        editName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), EditNameActivity.class);
                intent.putExtra("user_name", name.getText().toString());
                startActivity(intent);
            }
        });
        Log.e(TAG, "userid"+ userId);

        Glide.with(getActivity()).load("http://pre.schnotify.info/api/user_photo?user_id="+userId)
                .diskCacheStrategy(DiskCacheStrategy.NONE )
                .skipMemoryCache(true)
                .into(photo);

        displayProfile();

        browse.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                requestStoragePermission();

                String[] permission= {Manifest.permission.READ_EXTERNAL_STORAGE};
                int checkPermission = getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE);
                if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.M){
                    if (checkPermission != PackageManager.PERMISSION_DENIED){
                        requestPermissions(permission, READ_EXTERNAL_STORAGE_CODE);
                    }else {
                        openGallery();
                    }
                }else {

                    openGallery();

                }
            }
        });
    }

    void openGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(intent, OPEN_GALLERY);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == READ_EXTERNAL_STORAGE_CODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openGallery();
            }else {
                Toast.makeText(getActivity(), "Please grant permission", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uri = data.getData();
        String[] path={MediaStore.Images.Media.DATA};

       Cursor cursor = getActivity().getContentResolver().query(uri,path, null,null, null);
       if (cursor.moveToFirst()){
           int columnIndex = cursor.getColumnIndex(path[0]);
           imagePath = cursor.getString(columnIndex);

           update.setEnabled(true);
           photo.setImageURI(uri);

           Log.e(TAG,"path"+imagePath);
       }
    }

    void displayProfile(){



        Call<ResponseBody> userIDCall = parentService.getProfile(userId);
       userIDCall.enqueue(new Callback<ResponseBody>() {
           @Override
           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

               Log.e(TAG,"success"+response.body().toString());


           }

           @Override
           public void onFailure(Call<ResponseBody> call, Throwable t) {
            Log.e(TAG,"error");
           }
       });

    }


    private void requestStoragePermission() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            Toast.makeText(getActivity(), "All permissions are granted!", Toast.LENGTH_SHORT).show();
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(getActivity(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }




}
