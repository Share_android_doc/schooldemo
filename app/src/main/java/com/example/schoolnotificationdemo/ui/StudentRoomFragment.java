package com.example.schoolnotificationdemo.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.entity.StudentResponse;
import com.example.schoolnotificationdemo.helper.localDatabase.databaseCreater.AppDatabase;
import com.example.schoolnotificationdemo.helper.localDatabase.model.Student;

import com.example.schoolnotificationdemo.ui.adapter.StudentAdapterFromRoom;

import java.util.ArrayList;
import java.util.List;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class StudentRoomFragment extends Fragment {



    StudentAdapterFromRoom studentAdapterFromRoom;
    RecyclerView recyclerView;
    List<StudentResponse> studentResponseList;
    AppDatabase database;

    List<Student> students;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_notifications, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.homeRecyclerView);
        database= AppDatabase.getDatabaseInstance(getActivity());
        students = new ArrayList<>();
        setup();

    }

    void setup(){
        Log.e(TAG,"data"+database.studentDAO().getAll().toString());

        for (Student student: database.studentDAO().getAll()){
            students.add(student);
        }

        studentAdapterFromRoom = new StudentAdapterFromRoom(students, getContext().getApplicationContext());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(studentAdapterFromRoom);
    }
}
