package com.example.schoolnotificationdemo.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.schoolnotificationdemo.R;
import com.example.schoolnotificationdemo.api.network.ServiceGenerator;
import com.example.schoolnotificationdemo.api.service.SchoolService;
import com.example.schoolnotificationdemo.entity.UpdateNameResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditNameActivity extends AppCompatActivity {

    EditText userName;
    SchoolService.ParentService parentService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_name);
        userName = findViewById(R.id.username);
        Intent intent = getIntent();
        //userName.setText(intent.getStringExtra("user_name"));
        parentService = ServiceGenerator.createService(SchoolService.ParentService.class);
    }

    public void onSaveChange(View view) {

        Call<UpdateNameResponse> call = parentService.updateName(userName.getText().toString());
        call.enqueue(new Callback<UpdateNameResponse>() {
            @Override
            public void onResponse(Call<UpdateNameResponse> call, Response<UpdateNameResponse> response) {
                Log.e("success","sucess");
                Toast.makeText(EditNameActivity.this, "Username Update Successful", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<UpdateNameResponse> call, Throwable t) {
                Log.e("error", "error"+t.toString());
            }
        });

        Intent intent = new Intent(EditNameActivity.this, MainActivity.class);
        intent.putExtra("newusername",userName.getText().toString());
        startActivity(intent);


    }
}
