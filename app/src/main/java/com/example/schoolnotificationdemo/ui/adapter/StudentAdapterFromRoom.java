package com.example.schoolnotificationdemo.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schoolnotificationdemo.R;

import com.example.schoolnotificationdemo.helper.localDatabase.model.Student;

import java.util.List;

public class StudentAdapterFromRoom extends RecyclerView.Adapter<StudentAdapterFromRoom.MyViewHolder> {

    List<Student> studentList;

    Context context;
    LayoutInflater inflater;
    Student student;


    public StudentAdapterFromRoom(List<Student> studentResponseList, Context context) {
        this.studentList = studentResponseList;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.student_list_layout, null);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        student = studentList.get(position);
        holder.grade.setText(student.getLevel());
        holder.gender.setText(student.gender);
        holder.name.setText(student.getFullName());
    }

    @Override
    public int getItemCount() {
        return studentList.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder{

        ImageView studentPhoto;
        TextView name, gender, grade;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            studentPhoto = itemView.findViewById(R.id.photo);
            name = itemView.findViewById(R.id.tvName);
            gender = itemView.findViewById(R.id.tvGender);
            grade = itemView.findViewById(R.id.tvGrade);


        }
    }
}
