package com.example.schoolnotificationdemo.api.network;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {

    public final static String BASE_URL = "http://pre.schnotify.info/";

    public static String token;

    public ServiceGenerator(String token) {
        this.token = token;

        Log.e("eror",token.toString());
    }

   static  Gson gson = new GsonBuilder()
            .setLenient()
            .create();
    public static Retrofit.Builder builder = new Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL);

    public static <S> S createService(Class<S> classService){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .header("Content-type","application/json")
                        .method(original.method(), original.body());
                return chain.proceed(requestBuilder.build());
            }
        });

        Retrofit retrofit = builder.client(httpClient.build()).build();

        return retrofit.create(classService);

    }


}
