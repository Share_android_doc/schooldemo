package com.example.schoolnotificationdemo.api.service;

import android.media.Image;
import android.widget.ImageView;

import com.example.schoolnotificationdemo.entity.ParentResponse;
import com.example.schoolnotificationdemo.entity.StudentResponse;
import com.example.schoolnotificationdemo.entity.UpdateNameResponse;
import com.example.schoolnotificationdemo.entity.UserID;
import com.google.gson.JsonObject;


import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface SchoolService {

    interface ParentService {

        @FormUrlEncoded
        @POST("api/login")
        Call<ParentResponse> login(@Field("code") String code, @Field("tel") String tel, @Field("login_type") String type);



        @GET("api/user_photo")
        Call<ResponseBody>getProfile(@Query("user_id") String userID);

        @FormUrlEncoded
        @POST("api/update_mobile_user_name")
        Call<UpdateNameResponse>updateName(@Field("name") String username);

        @Multipart
        @POST("api/update/user-photo")
        Call<JsonObject> uploadImage(@Part MultipartBody.Part file);


    }

    interface StudentService {
        @GET("api/load_student")
        Call<List<StudentResponse>> getAllStudents();

    }
}
