package com.example.schoolnotificationdemo.helper.localDatabase.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.schoolnotificationdemo.helper.localDatabase.model.Student;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface StudentDAO {


    @Query("SELECT * FROM student")
    List<Student> getAll();

    @Insert(onConflict = REPLACE)
    void insertStudent(Student student);

    @Insert
    void insertAllStudents(Student... mStudentList);


}
