package com.example.schoolnotificationdemo.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class StorePreferences {
    SharedPreferences prefs;

    public StorePreferences(Context context) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
    }

    public void storeStringPreference(Context context, String key, String value) {

        SharedPreferences.Editor e = prefs.edit();
        e.putString(key, value);
        e.apply();
    }

    public String getStringPreference(Context context, String key) {

        return prefs.getString(key, "");
    }

    public void storeIntPreference(Context context, String key, int value) {

        SharedPreferences.Editor e = prefs.edit();
        e.putInt(key, value);
        e.apply();
    }

    public int getIntPreference(Context context, String key) {

        return prefs.getInt(key, 0);
    }

    public void storeBooleanPreference(Context context, String key,
                                       Boolean value) {

        SharedPreferences.Editor e = prefs.edit();
        e.putBoolean(key, value);
        e.apply();
    }

    public Boolean getBooleanPreference(Context context, String key) {

        return prefs.getBoolean(key, false);
    }

    public void storeLongPreference(Context context, String key, long value) {

        SharedPreferences.Editor e = prefs.edit();
        e.putLong(key, value);
        e.apply();
    }

    public long getLongPreference(Context context, String key) {

        return prefs.getLong(key, 0);
    }

    public void clearAllPref (){

        prefs.edit().clear().apply();


    }


}
