package com.example.schoolnotificationdemo.helper.localDatabase.model;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.schoolnotificationdemo.helper.localDatabase.DateConverter;

@Entity(tableName = "student")
@TypeConverters({DateConverter.class})
public class Student {
    @PrimaryKey(autoGenerate = true)
    public int sId;

    @ColumnInfo(name = "full_name")
    public String fullName;

    @ColumnInfo(name = "gender")
    public String gender;

    @ColumnInfo(name = "level")
    public String level;

    public Student() {
    }

    public Student(String fullName, String gender, String level) {
        this.fullName = fullName;
        this.gender = gender;
        this.level = level;
    }

    public int getsId() {
        return sId;
    }

    public void setsId(int sId) {
        this.sId = sId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sId=" + sId +
                ", fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                ", level='" + level + '\'' +
                '}';
    }
}
